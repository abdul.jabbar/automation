var express = require('express');
var router = express.Router();

const todos = [{id: 1, name: "Abdul", completed: true}]
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json(todos);
});

module.exports = router;
