const request = require('supertest');
const app = require('./app');

describe('Todos API', () => {
    it('GET todos ---> array todos', () => {
        return request(app).get('/todos').expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                expect(response.body).toEqual(expect.arrayContaining([
                    expect.objectContaining({
                        name: expect.any(String),
                        completed: expect.any(Boolean)
                    })
                ]))
            })
    })
    it('GET todos/id ---> specific todo by id', () => {

    })
    it('GET todos/id ---> 404 if not found', () => {

    })
    // it('POST /todos ---> creates todo', () => {
    //     return request(app).post('todos').send({
    //         name: 'do dishes'
    //     })
    //         .expect('Content-Type', /json/)
    //         .expect(201)
    //         .then(response => {
    //             expect(response.body).toEqual(
    //                 expect.objectContaining({
    //                     name: 'do dishes',
    //                     completed: false
    //                 }))
    //         })
    // })
    it('POST todos ---> validates request body', () => {

    })
}) //group your tests
